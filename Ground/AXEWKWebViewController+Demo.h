//
//  AXEWKWebViewController+Demo.h
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "AXEWKWebViewController.h"

// 定制webview ,处理返回按钮，以更好的支持单页面应用。
@interface AXEWKWebViewController (Demo)

+ (void)setupCustom;

@end
