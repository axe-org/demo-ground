//
//  DemoImagePickModule.m
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "DemoImagePickModule.h"
#import <UIKit/UIKit.h>
#import "AXEReactViewController.h"
#import <React/RCTBridge.h>
#import <React/RCTRootView.h>
#import "AXEReactControllerWrapper.h"
#import "AXEReactViewController.h"

@interface DemoImagePickModule() <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic,strong) AXEReactControllerWrapper *wrapper;
@property (nonatomic,copy) void (^finishBlock)(UIImage *);
@end


@implementation DemoImagePickModule
RCT_EXPORT_MODULE(ImagePicker);
@synthesize bridge = _bridge;
- (AXEReactControllerWrapper *)wrapper{
    if (!_wrapper) {
        _wrapper = [self.bridge.launchOptions objectForKey:AXEReactControllerWrapperKey];
    }
    return _wrapper;
}

+ (BOOL)requiresMainQueueSetup {
    return YES;
}

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}


// 关闭页面
RCT_EXPORT_METHOD(pickImage:(RCTResponseSenderBlock)successCallback){
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self.wrapper.controller presentViewController:picker animated:YES completion:nil];
    self.finishBlock = ^(UIImage *image){
        NSData *imageData = UIImageJPEGRepresentation(image, 0.8);// 默认0.8
        NSString *base64Data = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        successCallback(@[[@"data:image/jpeg;base64," stringByAppendingString:base64Data]]);
    };
}

// UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        self.finishBlock(image);
    }];
}


@end
